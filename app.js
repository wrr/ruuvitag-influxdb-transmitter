const ruuvi = require('node-ruuvitag')
const axios = require('axios')
const fs = require('fs')
const config = require('./config.js')
let measurements = []

const startTime = new Date()

const parseSingleData = data => {
  return `${config.database.measurement},tag=${data.id},tagName='${config.tags[data.id]}' temperature=${data.temperature},humidity=${data.humidity},pressure=${data.pressure}`
}

const sendData = data => {
    const d = parseSingleData(data)
    const promise = axios.post(`${config.database.url}/write?db=${config.database.database}&u=${config.database.username}&p=${config.database.password}`, d)
    promise.then(() => {
      console.log('Data sent succesfully')
      const now = new Date();
      if (config.transmitter.log) {
        fs.appendFile(config.transmitter.log, `${now.toISOString()} ${d}\n`, () => {})
      }
    }).catch(err => {
      console.log('Sending data failed', err)
    })
}

const myInterval = setInterval(() => {
  const currentTime = new Date()
  if (currentTime.valueOf() > (startTime.valueOf() + (config.transmitter.runningTime * 1000))) {
    process.exit(1)
  }
}, 2000)

const databasePromise = axios.get(`${config.database.url}/ping`)

databasePromise.then(() => {
  console.log('Database found!')

  // MAIN LOOP
  ruuvi.on('found', tag => {
    console.log('Found tag:', config.tags[tag.id])
    tag.on('updated', data => {
      if (!measurements[tag.id]) {
        measurements[tag.id] = data
        measurements[tag.id].id = tag.id
        sendData(measurements[tag.id])
      }
    })
  })
}).catch(() => {
  console.log('Database not found, exiting...')
})


const ruuvi = require('node-ruuvitag')
const axios = require('axios')
const fs = require('fs')
const config = require('./config.js')
let measurements = []

const parseSingleData = (tagId, data) => {
  return `${config.tags[tagId]}: temperature=${data.temperature},humidity=${data.humidity},pressure=${data.pressure}`
}

const startTime = new Date()

const myInterval = setInterval(() => {
  const currentTime = new Date()
  if (currentTime.valueOf() > (startTime.valueOf() + (config.transmitter.runningTime * 1000))) {
    process.exit(1)
  }
}, 2000)

const databasePromise = axios.get(`${config.database.url}/ping`)

console.log('\n\n\nruuvitag-influxdb-transmitter tester')
console.log('\nConfig:\n')
console.log('Tags:', config.tags)
databasePromise.then(() => {
console.log('\n\nDatabase config OK, database found!\n\n')

  // MAIN LOOP
  ruuvi.on('found', tag => {
    console.log('Found tag:', config.tags[tag.id])
    tag.on('updated', data => {
      console.log(parseSingleData(tag.id, data))
    })
  })
}).catch(() => {
  console.log('\n\nDatabase not found, exiting...')
  process.exit(1)
})


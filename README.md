# ruuvitag-influxdb-transmitter

Reads ruuvitag data from bluetooth using node-ruuvitag library. Sends data to InfluxDB API endpoint. Works (at least) on Raspberry Pi 3B+.

# Usage

Copy config.js.example to config.js and fill in your database credentials etc. Put the script to cron or run manually and enjoy!

You can test that your database connection works and that you get readings from the tags by running test.js.

Cron example:
```
*/5 * * * * sudo node /home/pi/ruuvitag-influxdb-transmitter/app.js
```

# Requirements

Tested on Raspberry Pi 3B+ and Raspberry Pi 4B+. With Raspberry Pi 3 you might want to reduce Bluetooth speed as described here https://f.ruuvi.com/t/spikes-in-data-collected-with-rpi3/3669/5

# Dependencies

- https://github.com/pakastin/node-ruuvitag
- https://github.com/axios/axios
